﻿using BLL;
using DAL.Repository.Repositories;
using DAL.Repository.UnitOfWork;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.CustomModels;
using Models.DTO;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore2.Tests.Service
{
    [TestClass]
    public class BookServiceTest
    {
        private IBookService _bookService;
        IBookRepository _bookRepository;
        Mock<IUnitOfWork> _mockUnitOfWork;


        [TestInitialize]
        public void Initialize()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _bookRepository = new BookRepository(new DAL.DataContext.BookContext());
            _bookService = new BookService(_mockUnitOfWork.Object, _bookRepository);
        }


        [TestMethod]
        public void GetAll()
        {
            IEnumerable<Book> results = _bookService.GetAll();
            Assert.IsNotNull(results);
        }


        [TestMethod]
        public void GetById()
        {
            Book results = _bookService.GetById(1);
            Assert.IsNotNull(results);
        }

    }
}

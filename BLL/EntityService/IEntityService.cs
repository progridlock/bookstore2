﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.EntityService
{
    public interface IService
    {

    }

    public class Service:IService
    {

    }

    public interface IEntityService<T> : IService where T : BaseEntity
    {
        void Create(T entity);
        void Delete(T entity);
        IEnumerable<T> GetAll();
        void Update(T entity);
    }
}

﻿using BLL.EntityService;
using DAL.Repository.Repositories;
using DAL.Repository.UnitOfWork;
using Models.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public class PurchaseService : EntityService<Purchase>, IPurchaseService
    {
        IPurchaseRepository _purchaseRepository;

        public PurchaseService(IPurchaseRepository purchaseRepository) : base(purchaseRepository)
        {
            _purchaseRepository = purchaseRepository;
        }

        public void AddPurchase(int userId, int bookId)
        {
            Purchase purchase = new Purchase() { UserId = userId, BookId = bookId , PurchaseDate = DateTime.Now};
            Create(purchase);
        }

    }
}

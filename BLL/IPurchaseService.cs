﻿using BLL.EntityService;
using Models.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public interface IPurchaseService: IEntityService<Purchase>
    {
        void AddPurchase(int userId, int bookId);
    }
}

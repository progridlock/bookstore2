﻿using BLL.EntityService;
using Models.DTO;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public interface IBookService: IEntityService<Book>
    {
        Book GetById(int id);
        Book Create(BookModel bookModel);
    }
}

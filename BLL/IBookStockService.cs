﻿using BLL.EntityService;
using Models.DTO;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public interface IBookStockService : IEntityService<BookStock>
    {
        bool DecreaseStock(PurchaseDetails purchaseDetails);
    }
}

﻿using BLL.EntityService;
using Common.CustomHelpers;
using DAL.Repository.Repositories;
using DAL.Repository.UnitOfWork;
using Flurl.Http;
using Models.CustomModels;
using Models.DTO;
using Models.UserModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace BLL
{
    public class UserService : EntityService<User>, IUserService
    {
       // IUnitOfWork _unitOfWork;
        IUserRepository _userRepository;

        public UserService(/*IUnitOfWork unitOfWork, */IUserRepository userRepository) : base(userRepository)
        {
            //_unitOfWork = unitOfWork;
            _userRepository = userRepository;
        }


        public User GetById(int id)
        {
            return _userRepository.GetById(id);
        }

        public User GetByEmail(string email)
        {
            return _userRepository.GetByEmail(email);
        }

        public bool IsUserExist(string email)
        {            
            return _userRepository.IsUserExist(email);
        }

        public void RegisterUser(UserModel userModel)
        {
            string userPasswordHash = ComputeHash.Encrypt(userModel.Password);// implement your own logic, it's just a sample 
            string userPasswordSalt = "2cf24dba5fb0a30e"; // implement your own logic, it's just a sample 

            User user = new User()
            {
                Email = userModel.Email,
                PasswordHash = userPasswordHash,
                PasswordSalt = userPasswordSalt,
                RegisteredDate = DateTime.Now,
                RegisteredIp = "192.168.133.111",
                IsActive = true

            };

            Create(user);
        }

        public async Task<string> Authenticate(LoginUserModel loginUserModel)
        {


            User user = _userRepository.GetByEmail(loginUserModel?.Email.Trim());

            if (user == null) return null;

            bool isValidPassword = ComputeHash.IsValidPassword(loginUserModel?.Password, user?.PasswordHash);

            if (!isValidPassword) return null;

            if (isValidPassword)
            {
                return await RequestToken.GetTokenAsync(user.Email);
            }

            return null;
           
        }

    }
}


﻿using BLL.EntityService;
using DAL.Repository.Repositories;
using DAL.Repository.UnitOfWork;
using Models.DTO;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public class BookStockService : EntityService<BookStock>, IBookStockService
    {
        IBookStockRepository _bookStockRepository;

        public BookStockService(IBookStockRepository bookStockService) : base(bookStockService)
        {
            _bookStockRepository = bookStockService;
        }

        public bool DecreaseStock(PurchaseDetails purchaseDetails)
        {
            bool result=false;

            BookStock bookStock = _bookStockRepository.GetById(purchaseDetails.StockId, purchaseDetails.BookId);
            if (bookStock.BookCount > 0 && (bookStock.BookCount - purchaseDetails.BookCount) >= 0)
            {
                bookStock.BookCount = bookStock.BookCount - purchaseDetails.BookCount;
                Update(bookStock);
                result = true;
            }

            return result;
           
        }
    }
}

﻿using BLL.EntityService;
using Models.DTO;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IUserService : IEntityService<User>
    {
        User GetById(int id);
        User GetByEmail(string email);
        void RegisterUser(UserModel userModel);

        bool IsUserExist(string email);

        Task<string> Authenticate(LoginUserModel loginUserModel);

    }
}

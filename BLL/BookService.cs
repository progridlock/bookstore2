﻿using BLL.EntityService;
using DAL.Repository.Repositories;
using DAL.Repository.UnitOfWork;
using Models.DTO;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public class BookService : EntityService<Book>, IBookService
    {
        IUnitOfWork _unitOfWork;
        IBookRepository _bookRepository;


        public BookService(IUnitOfWork unitOfWork, IBookRepository bookRepository) : base(bookRepository)
        {
            _unitOfWork = unitOfWork;
            _bookRepository = bookRepository;
        }

        public Book Create(BookModel bookModel)
        {
            Book book = new Book()
            {
                Name = bookModel.Name,
                Description = bookModel.Description
            };

            Create(book);

            return book;
        }

        public Book GetById(int id)
        {
            return _bookRepository.GetById(id);
        }




    }
}

﻿using BLL.EntityService;
using Models.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public interface IStockService : IEntityService<Stock>
    {
        Stock GetById(int id);
        Stock GetAnyStock();
    }
}

﻿using BLL.EntityService;
using DAL.Repository.Repositories;
using Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class StockService : EntityService<Stock>, IStockService
    {
        IStockRepository _stockRepository;
        public StockService(IStockRepository stockRepository) : base(stockRepository)
        {
            _stockRepository = stockRepository;
        }

        public Stock GetById(int id)
        {
            return _stockRepository.GetById(id);
        }

        public Stock GetAnyStock()
        {
            IQueryable<Stock> stocks = _stockRepository.GetAll() as IQueryable<Stock>;           
            return stocks.FirstOrDefault();
        }
    }
}

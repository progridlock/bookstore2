﻿using BLL;
using BLL.EntityService;
using DAL.DataContext;
using DAL.Repository.Repositories;
using DAL.Repository.UnitOfWork;
using Ninject;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using WebAPI2.Controllers;

namespace WebAPI2.App_Start
{
    public static class NinjectConfig
    {
        public static Lazy<IKernel> CreateKernel = new Lazy<IKernel>(() =>
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());

            RegisterServices(kernel);

            return kernel;
        });

        private static void RegisterServices(KernelBase kernel)
        {
            kernel.Bind<DbContext>().To<BookContext>().InSingletonScope();
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InSingletonScope();

            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<IBookService>().To<BookService>();
            kernel.Bind<IBookRepository>().To<BookRepository>();
            kernel.Bind<IPurchaseService>().To<PurchaseService>();
            kernel.Bind<IPurchaseRepository>().To<PurchaseRepository>();
            kernel.Bind<IBookStockService>().To<BookStockService>();
            kernel.Bind<IBookStockRepository>().To<BookStockRepository>();
            kernel.Bind<IStockService>().To<StockService>();
            kernel.Bind<IStockRepository>().To<StockRepository>();


        }
    }
}
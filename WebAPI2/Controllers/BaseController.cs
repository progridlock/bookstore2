﻿using BLL;
using Models.DTO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Resources;
using System.Security.Claims;
using System.Web.Http;
using Resources = Models.Resources.ContentTranslation;


namespace WebAPI2.Controllers
{
    public class BaseController : ApiController
    {

        private string lang;
        IUserService _userService;



        public BaseController(IUserService userService)
        {
            _userService = userService;
        }


        public string Lang
        {
            get
            {
                lang = Request.Headers.AcceptLanguage.FirstOrDefault()?.Value;
                return lang != null ? lang : "en";
            }
        }


        public User CurrentUser
        {
            get
            {
                User user = null;
                var userWithClaims = (ClaimsPrincipal)User;
                var email = userWithClaims?.Claims?.FirstOrDefault()?.Value;

                if (email != null)
                {
                    user = _userService.GetByEmail(email);
                }

                return user;
            }
        }


        public string GetContentTranslation(string lang)
        {

            var rm = new ResourceManager(typeof(Resources));
            string translation = rm.GetString("SuccessMessage", CultureInfo.CurrentCulture);
            return translation;
        }














    }
}




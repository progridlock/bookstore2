﻿using BLL;
using DAL.Repository.UnitOfWork;
using Models.CustomModels;
using Models.DTO;
using Models.FluentValidation;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using Resources =  Models.Resources.ContentTranslation;

namespace WebAPI2.Controllers
{
    [Authorize]
    [RoutePrefix("api/account")]
    public class AccountController : BaseController
    {
        IUserService _userService;
        IUnitOfWork _unitOfWork;

        public AccountController(IUserService userService, IUnitOfWork unitOfWork) : base(userService)
        {
            _userService = userService;
            _unitOfWork = unitOfWork;
        }


        [HttpGet]
        [Route("getbyid/{id}")]
        public IHttpActionResult GetByID(int id)
        {
            User user = _userService.GetById(id);
            return Ok(user);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("registeruser")]
        public IHttpActionResult RegisterUser(UserModel userModel)
        {
            RequestResult requestResult = new RequestResult();

            try
            {
                UserModelValidation validator = new UserModelValidation();

                var validationResult = validator.Validate(userModel);

                if (!validationResult.IsValid)
                {
                    requestResult.ErrorMessage = validationResult.Errors.Select(x => x.ToString()).ToList();
                    return Ok(requestResult);
                }

                bool isUserExist = _userService.IsUserExist(userModel.Email.Trim());

                if (isUserExist) { requestResult.ErrorMessage = new List<string>() { Resources.UserExist }; return Ok(requestResult); }//

                _userService.RegisterUser(userModel);
                _unitOfWork.Commit();

                requestResult.Result = true;
                requestResult.SuccessMessage = "Successful";
                return Ok(requestResult);
            }
            catch (Exception ex)
            {
                //requestResult.Result = false;
                return Ok(requestResult);
                //TODO Logging
            }


        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginUserModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("Authenticate")]
        public async Task<IHttpActionResult> Authenticate(LoginUserModel loginUserModel)
        {
            AuthRequestResult authRequestResult = new AuthRequestResult();

            try
            {
                var tokenCredentials = await _userService.Authenticate(loginUserModel);
                if (tokenCredentials == null)
                {
                    authRequestResult.ErrorMessage = new List<string>() { Resources.EmailOrPasswordIncorrect };
                }
                else
                {
                    authRequestResult.Result = true;
                    authRequestResult.Token = tokenCredentials;
                }

            }
            catch (Exception ex)
            {
                authRequestResult.Result = false;
                authRequestResult.ErrorMessage = new List<string>() { Resources.GeneralError };
                //TODO Logging
            }

            return Ok(authRequestResult);
        }
    }

}

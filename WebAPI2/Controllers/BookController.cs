﻿using BLL;
using DAL.Repository.UnitOfWork;
using Models.CustomModels;
using Models.DTO;
using Models.FluentValidation;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Resources;
using System.Security.Claims;
using System.Web.Http;
using Resources = Models.Resources.ContentTranslation;


namespace WebAPI2.Controllers
{
    [Authorize]
    [RoutePrefix("api/book")]
    public class BookController : BaseController
    {
        IBookService _bookService;
        IPurchaseService _purchaseService;
        IBookStockService _bookStockService;
        IStockService _stockService;
        IUserService _userService;
        IUnitOfWork _unitOfWork;

        public BookController(IBookService bookService, IPurchaseService purchaseService,
                              IBookStockService bookStockService, IStockService stockService, IUserService userService,
                              IUnitOfWork unitOfWork) : base(userService)
        {
            _bookService = bookService;
            _purchaseService = purchaseService;
            _bookStockService = bookStockService;
            _userService = userService;
            _stockService = stockService;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getbyid/{id}")]
        public IHttpActionResult GetByID(int id)
        {
            Book book = _bookService.GetById(id);
            return Ok(book);
        }


        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(BookModel bookModel)
        {
            RequestResult requestResult = new RequestResult();

            try
            {
                BookModelValidation validator = new BookModelValidation();

                var validationResult = validator.Validate(bookModel);

                if (!validationResult.IsValid)
                {
                    requestResult.ErrorMessage = validationResult.Errors.Select(x => x.ToString()).ToList();
                    return Ok(requestResult);
                }

                //if stock is null, create new --that's just for test
                Stock stock = _stockService.GetAnyStock();

                if (stock == null)
                {
                    stock = new Stock() {Name = "London Stock" };
                    _stockService.Create(stock);
                }

                Book book = _bookService.Create(bookModel);
                _bookStockService.Create(new BookStock() { BookId = book.Id, StockId = stock.Id, BookCount = bookModel.BookCount });
                _unitOfWork.Commit();

                requestResult.Result = true;
                requestResult.SuccessMessage = Resources.SuccessMessage;

            }
            catch (Exception ex)
            {
                //TODO: Logging
            }

            return Ok(requestResult);

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="purchaseDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("purchase")]
        public IHttpActionResult Purchase(PurchaseDetails purchaseDetails)
        {
            RequestResult requestResult = new RequestResult();

            try
            {
                PurchaseDetailsModelValidation validator = new PurchaseDetailsModelValidation();

                var validationResult = validator.Validate(purchaseDetails);

                if (!validationResult.IsValid)
                {
                    requestResult.ErrorMessage = validationResult.Errors.Select(x => x.ToString()).ToList();
                    return Ok(requestResult);
                }



                bool isValid = _bookStockService.DecreaseStock(purchaseDetails);
                if (isValid)
                {
                    _purchaseService.AddPurchase(CurrentUser.Id, purchaseDetails.BookId);
                    _unitOfWork.Commit();

                    requestResult.Result = true;
                    requestResult.SuccessMessage = Resources.SuccessMessage;


                }

            }
            catch (Exception ex)
            {
                //TODO: Logging
            }

            return Ok(requestResult);

        }

        [HttpGet]
        [Route("getall")]
        public IHttpActionResult GetAll()
        {
            ListEntitiesResult<Book> listEntitiesResult = new ListEntitiesResult<Book>();


            try
            {
                IEnumerable<Book> books = _bookService.GetAll();
                listEntitiesResult.Result = true;
                listEntitiesResult.Entities = books.ToList();

            }
            catch (Exception ex)
            {
                listEntitiesResult.ErrorMessage = new List<string>() { Resources.GeneralError };
                // TODO: Logging
            }

            return Ok(listEntitiesResult);
        }
    }
}

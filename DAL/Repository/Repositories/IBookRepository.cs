﻿using DAL.Repository.GenericRepository;
using Models.DTO;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository.Repositories
{
    public interface IBookRepository : IGenericRepository<Book>
    {
        Book GetById(int id);
        bool IsPurchased(PurchaseDetails purchaseDetails);

    }
}

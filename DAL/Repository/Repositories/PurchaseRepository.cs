﻿using DAL.Repository.GenericRepository;
using Models.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace DAL.Repository.Repositories
{
    public class PurchaseRepository : GenericRepository<Purchase>, IPurchaseRepository
    {
        public PurchaseRepository(DbContext context) : base(context) { }

    }
}

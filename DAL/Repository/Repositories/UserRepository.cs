﻿using DAL.Repository.GenericRepository;
using Models.DTO;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace DAL.Repository.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
       
        public UserRepository(DbContext context) : base(context) { }

        public User GetByEmail(string email)
        {
            return FindBy(x => x.Email == email).FirstOrDefault();
        }

        public User GetById(int id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public bool IsUserExist(string email)
        {
            return FindBy(x => x.Email == email).Any();
        }

    }
}

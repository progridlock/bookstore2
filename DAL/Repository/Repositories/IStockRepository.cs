﻿using DAL.Repository.GenericRepository;
using Models.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository.Repositories
{
    public interface IStockRepository: IGenericRepository<Stock>
    {
        Stock GetById(int id);
    }
}

﻿using DAL.Repository.GenericRepository;
using Models.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace DAL.Repository.Repositories
{
    public class StockRepository: GenericRepository<Stock>, IStockRepository
    {
        public StockRepository(DbContext context) : base(context) { }

        public Stock GetById(int id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
    }
}

﻿using DAL.Repository.GenericRepository;
using Models.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace DAL.Repository.Repositories
{
    public class BookStockRepository : GenericRepository<BookStock>, IBookStockRepository
    {
        public BookStockRepository(DbContext context) : base(context) { }

        public BookStock GetById(int stockId, int bookId)
        {
            return FindBy(x => x.StockId == stockId&&x.BookId==bookId).FirstOrDefault();
        }

    
    }
}

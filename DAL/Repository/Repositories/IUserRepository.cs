﻿using DAL.Repository.GenericRepository;
using Models.DTO;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository.Repositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
        User GetById(int id);
        User GetByEmail(string email);
        bool IsUserExist(string email);
    }
}

﻿using DAL.Repository.GenericRepository;
using Models.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository.Repositories
{
    public interface IBookStockRepository : IGenericRepository<BookStock> 
    {
        BookStock GetById(int stockId, int bookId);
    }
}

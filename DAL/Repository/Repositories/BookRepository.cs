﻿using DAL.Repository.GenericRepository;
using Models.DTO;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace DAL.Repository.Repositories
{
   public class BookRepository: GenericRepository<Book>, IBookRepository
    {
        public BookRepository(DbContext context) : base(context) { }

        public Book GetById(int id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public bool IsPurchased(PurchaseDetails purchaseDetails)
        {
            return false;
        }
    }
}

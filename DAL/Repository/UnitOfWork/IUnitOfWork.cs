﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        int Commit();
    }
}

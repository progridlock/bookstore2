﻿using Models.DTO;
using Models.Entity;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Threading;

namespace DAL.DataContext
{
    public class BookContext : DbContext
    {
        public BookContext() : base("Name=BookContext") { Database.SetInitializer(new BookDBInitializer()); }

        public DbSet<User> Users { get; set; }

        public DbSet<Book> Books { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<Stock> Stocks { get; set; }

        public DbSet<BookStock> BookStocks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();


            modelBuilder.Entity<BookStock>()
                .HasKey(c => new { c.StockId, c.BookId });

            modelBuilder.Entity<BookStock>()
                .HasRequired(d => d.Book)
                .WithMany(p => p.BookStock)
                .HasForeignKey(d => d.BookId);

            modelBuilder.Entity<BookStock>()
                .HasRequired(d => d.Stock)
                .WithMany(p => p.BookStock)
                .HasForeignKey(d => d.StockId);


            modelBuilder.Entity<Book>()
                .Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(300);

            modelBuilder.Entity<Book>()
                .Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(100);


            modelBuilder.Entity<Purchase>()
                .HasRequired(d => d.Book)
                .WithMany(p => p.Purchase)
                .HasForeignKey(d => d.BookId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Purchase>()
                .HasRequired(p => p.User)
                .WithMany(p => p.Purchase)
                .HasForeignKey(d => d.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(300);

            modelBuilder.Entity<User>()
               .Property(e => e.PasswordHash)
               .IsRequired()
               .HasMaxLength(300);

            modelBuilder.Entity<User>()
                .Property(e => e.PasswordSalt)
                .IsRequired()
                .HasMaxLength(300);

            modelBuilder.Entity<User>()
                .Property(e => e.RegisteredIp)
                .IsRequired();
        }

        public override int SaveChanges()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableEntity
                    && (x.State == System.Data.Entity.EntityState.Added || x.State == System.Data.Entity.EntityState.Modified));

            foreach (var entry in modifiedEntries)
            {
                IAuditableEntity entity = entry.Entity as IAuditableEntity;
                if (entity != null)
                {
                    string identityName = Thread.CurrentPrincipal.Identity.Name;
                    DateTime now = DateTime.UtcNow;

                    if (entry.State == System.Data.Entity.EntityState.Added)
                    {
                        entity.CreatedBy = identityName;
                        entity.CreatedDate = now;
                    }
                    else
                    {
                        base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                    }

                    entity.UpdatedBy = identityName;
                    entity.UpdatedDate = now;
                }
            }

            int result = base.SaveChanges();

            return result;
        }
    }
}

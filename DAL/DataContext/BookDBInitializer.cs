﻿using Models.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using System.Linq;

namespace DAL.DataContext
{
    public class BookDBInitializer: DropCreateDatabaseAlways<BookContext>
    {
        protected override void Seed(BookContext context)
        {
            IList<Stock> defaultStocks = new List<Stock>();

            defaultStocks.Add(new Stock() { Name = "London" });

            context.Stocks.AddRange(defaultStocks);


            IList<Book> defaultBooks = new List<Book>();

            defaultBooks.Add(new Book() { Name = "Advanced Algorithms", Description = "Advanced Algorithms" });
            defaultBooks.Add(new Book() { Name = "Fundamentals of Computer Science", Description = "Fundamentals of Computer Science" });
            defaultBooks.Add(new Book() { Name = "Data Visualisation", Description = "Data Visualisation" });
            defaultBooks.Add(new Book() { Name = "Data Mining", Description = "Data Mining" });
            defaultBooks.Add(new Book() { Name = "Data Science", Description = "Data Science" });

            context.Books.AddRange(defaultBooks);

            IList<BookStock> defaultBookStocks = new List<BookStock>();

            Random r = new Random();
            int id = 1;

            foreach (Book book in defaultBooks)
            {
                defaultBookStocks.Add(new BookStock() { BookId = id, BookCount = r.Next(10, 100), StockId = defaultStocks.FirstOrDefault().Id });
                id = ++id;
            }

            context.BookStocks.AddRange(defaultBookStocks);


            base.Seed(context);
        }
    }
}

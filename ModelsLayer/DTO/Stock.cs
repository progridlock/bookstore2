﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.DTO
{
    public class Stock : Entity<int>
    {
        public Stock()
        {
            BookStock = new HashSet<BookStock>();
        }
        public string Name { get; set; }

        public ICollection<BookStock> BookStock { get; set; }
    }
}

﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.DTO
{
    public class User : Entity<int>
    {
        public User()
        {
            Purchase = new HashSet<Purchase>();
        }

        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public DateTime RegisteredDate { get; set; }
        public string RegisteredIp { get; set; }
        public bool IsActive { get; set; }

        public ICollection<Purchase> Purchase { get; set; }
    }
}

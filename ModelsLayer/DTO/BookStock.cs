﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.DTO
{
    public class BookStock : BaseEntity
    {
        public int StockId { get; set; }
        public int BookId { get; set; }
        public int BookCount { get; set; }

        public Book Book { get; set; }
        public Stock Stock { get; set; }
    }
}

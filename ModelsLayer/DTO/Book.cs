﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.DTO
{
    public class Book : Entity<int>
    {
        public Book()
        {
            BookStock = new HashSet<BookStock>();
            Purchase = new HashSet<Purchase>();
        }

        public string Name { get; set; }
        public string Description { get; set; }



        public virtual ICollection<BookStock> BookStock { get; set; }
        public ICollection<Purchase> Purchase { get; set; }

        //public virtual ICollection<Purchase> Purchase { get; set; }
    }
}

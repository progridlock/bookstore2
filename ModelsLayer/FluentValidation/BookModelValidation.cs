﻿using FluentValidation;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.FluentValidation
{
    public class BookModelValidation: AbstractValidator<BookModel>
    {
        public BookModelValidation()
        {
            RuleFor(b => b.Name).NotEmpty().MaximumLength(100);
            RuleFor(b => b.Description).NotEmpty().MaximumLength(300);
        }
    }
}

﻿using FluentValidation;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.Text;


namespace Models.FluentValidation
{
    public class UserModelValidation : AbstractValidator<UserModel>
    {
        public UserModelValidation()
        {
            RuleFor(c => c.Email).EmailAddress().NotEmpty().WithMessage(Resources.ContentTranslation.EmailRequired);

            RuleFor(c => c.Password).NotEmpty().WithMessage(Resources.ContentTranslation.PasswordRequired)
                                               .Length(6, 30).WithMessage(Resources.ContentTranslation.PasswordLength);

            RuleFor(c => c.ConfirmPassword).Equal(c => c.Password)
                .When(c => !string.IsNullOrWhiteSpace(c.Password))
                .WithMessage(Resources.ContentTranslation.ConfirmPassword);
        }
    }
}

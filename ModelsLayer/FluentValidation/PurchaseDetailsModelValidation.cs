﻿using FluentValidation;
using Models.UserModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.FluentValidation
{
    public class PurchaseDetailsModelValidation: AbstractValidator<PurchaseDetails>
    {
        public PurchaseDetailsModelValidation()
        {
            RuleFor(p => p.BookCount).GreaterThan(0).WithMessage(Resources.ContentTranslation.BookCountZeroThan);
            
        }
    }
}

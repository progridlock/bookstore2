﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.UserModels
{
    public class BookModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int StockId { get; set; }
        public int BookCount { get; set; }
    }
}

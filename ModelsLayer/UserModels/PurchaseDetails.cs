﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.UserModels
{
    public class PurchaseDetails
    {
        public int BookId { get; set; }
        public int StockId { get; set; } = 1;
        public int BookCount { get; set; }
        public DateTime PurchaseDate { get; set; } = DateTime.Now;
    }
}

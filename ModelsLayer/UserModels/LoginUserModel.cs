﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.UserModels
{
    public class LoginUserModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}

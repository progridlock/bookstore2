﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.CustomModels
{
    public class ListEntitiesResult<T>: RequestResult
    {
        public IList<T> Entities { get; set; }
    }
}

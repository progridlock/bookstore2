﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.CustomModels
{
    public class RequestResult
    {
        public bool Result { get; set; }
        public string SuccessMessage { get; set; }
        public IEnumerable<string> ErrorMessage { get; set; }
        public int ErrorCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.CustomModels
{
    public class AuthRequestResult : RequestResult
    {
        public string Token { get; set; }
    }
}

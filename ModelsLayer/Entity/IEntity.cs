﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entity
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entity
{
    public abstract class Entity<T> : BaseEntity, IEntity<T>
    {
        public virtual T Id { get; set; }
    }
}

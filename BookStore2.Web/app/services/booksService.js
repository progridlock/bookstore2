﻿'use strict';
app.factory('booksService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var booksServiceFactory = {};

    var _getBooks = function () {

        return $http.get(serviceBase + 'api/book/getall').then(function (results) {
            return results;
        });
    };

    var _purchaseBooks = function (book, purchaseCount, lang) {

        var data = "BookId=" + book.id + "&BookCount=" + purchaseCount + "&StockId=1";

        return $http.post(serviceBase + 'api/book/purchase', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Accept-Language': lang } }).success(function (response) {
            return response;
        });






    };

    booksServiceFactory.getBooks = _getBooks;
    booksServiceFactory.purchaseBooks = _purchaseBooks;

    return booksServiceFactory;

}]);
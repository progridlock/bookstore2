﻿'use strict';
app.factory('adminsService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var adminsServiceFactory = {};

    var _getAllBooks = function () {

        return $http.get(serviceBase + 'api/book/getall').then(function (results) {
            return results;
        });
    };


    adminsService.getAllBooks = _getAllBooks;

    return adminsServiceFactory;

}]);
﻿'use strict';
app.controller('booksController', ['$scope', 'booksService', function ($scope, booksService) {

    $scope.books = [];

    var books = function () {
        booksService.getBooks().then(function (results) {

            $scope.books = results.data.entities;

        }, function (error) {
            //alert(error.data.message);
        });
    };

    books();


    $scope.go = function (book, purchaseCount) {

        if (purchaseCount === undefined || purchaseCount === "") {
            //alert("Please, enter book count!");
            return;
        }

        var lang = $(".langActive").text();

        booksService.purchaseBooks(book, purchaseCount, lang).then(function (results) {

            var requestResult = JSON.stringify(results.data);

            if (!requestResult === "true") { alert("Error"); return; }

            alert(results.data.successMessage);

            books(); 


        }, function (error) {
            //alert(error.data.message);
        });

    };

}]);
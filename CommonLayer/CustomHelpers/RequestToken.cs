﻿using Models.CustomModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Common.CustomHelpers
{
    public class RequestToken
    {
        public static async Task<string> GetTokenAsync(string email)
        {
            var request = (HttpWebRequest)WebRequest.Create("http://localhost:57648/token");

            var postData = $"grant_type=password&username={email}";

            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            Token token = new Token();

            Token tokenCredentials = JsonConvert.DeserializeObject<Token>(responseString);

            return tokenCredentials?.AccessToken;
        }
    }
}
